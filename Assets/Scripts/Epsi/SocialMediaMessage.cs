﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SocialMediaMessage
{

    [SerializeField]
    private string lastname;


    [SerializeField]
    private string firstname;


    [SerializeField]
    private string phone;


    [SerializeField]
    private string email;

    public SocialMediaMessage(string Nom, string Prenom, string Telephone, string Mail)
    {
        lastname = Nom;
        firstname = Prenom;
        phone = Telephone;
        email = Mail;
    }


    public string Lastname { get => lastname; set => lastname = value; }


    public string Firstname { get => firstname; set => firstname = value; }

  
    public string Phone { get => phone; set => phone = value; }


    public string Email { get => email; set => email = value; }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;
using TMPro;

/// <summary>
/// Represents the action for the promo codes happening to the application view
/// </summary>
public class PromoCodeManager : MonoBehaviour
{
    /// <summary>
    /// The GameObject of the promo code button that we are going to be attached to this.gameObject
    /// </summary>
    public GameObject ButtonPrefab;

    /// <summary>
    /// The parent GameObject of all modals. Modals will be attached as child to this.
    /// </summary>
    public GameObject ModalContainer;

    /// <summary>
    /// The GameObject of the modal we are going to link to the promo code button
    /// Will contain the promo code information
    /// </summary>
    public GameObject ModalPrefab;

    /// <summary>
    /// Create an instance of our promo code button on the application view
    /// </summary>
    /// <returns>The response as a GameObject</returns>
    public GameObject instantiateButton()
    {
        return Instantiate(ButtonPrefab, transform);
    }

    /// <summary>
    /// Create an instance of our modal on the application view
    /// </summary>
    /// <returns>The response as a GameObject</returns>
    public GameObject instantiateModal()
    {
        return Instantiate(ModalPrefab, ModalContainer.transform);
    }

    /// <summary>
    /// Take a button object and a string and write the passed string parameter on the button text.
    /// </summary>
    /// <param name="Button">The Button gameobject on the view</param>
    /// <param name="ButtonText">The text we want to print on the button</param>
    public void settingButton(GameObject Button, string ButtonText)
    {
        GameObject TextNormal = Button.transform.GetChild(0).GetChild(1).gameObject;
        GameObject TextHighlighted = Button.transform.GetChild(1).GetChild(1).gameObject;
        TextNormal.GetComponent<TextMeshProUGUI>().text = ButtonText;
        TextHighlighted.GetComponent<TextMeshProUGUI>().text = ButtonText;
    }

    /// <summary>
    /// Take a modal object and differents stringsand write the passed strings parameters on the modal text.
    /// </summary>
    /// <param name="Modal">The Modal gameobject on the view</param>
    /// <param name="ModalTitle">The text we want to print on the modal title</param>
    /// <param name="ModalFirstLine">The text we want to print on the first line of the modal</param>
    /// <param name="ModalSecondLine">The text we want to print on the second line of the modal</param>
    public void settingModal(GameObject Modal, string ModalTitle, string ModalFirstLine = null, string ModalSecondLine = null)
    {
        ModalWindowManager modalWindowManager = Modal.GetComponent<ModalWindowManager>();
        modalWindowManager.titleText = ModalTitle;
        string Description = "";
        if(ModalFirstLine != null && ModalSecondLine != null)
        {
            Description = ModalFirstLine + " \n " + ModalSecondLine;
        }
        else if(ModalFirstLine != null)
        {
            Description = ModalFirstLine;
        }
        modalWindowManager.descriptionText = Description;
        modalWindowManager.UpdateUI();
    }

    /// <summary>
    /// Take a Button and a Modal and add a listener the modal "delete" button
    /// The listener will trigger the deletion of Button promo code gameobject along with modal gameobject
    /// To do so, it'll call deletePromoCode methode on PromoCodeData, which will manage the data for us.
    /// </summary>
    /// <param name="Modal">The Modal gameobject</param>
    /// <param name="Button">The Button gameobject</param>
    public IEnumerator bindModalDeleteButton(GameObject Modal, GameObject Button)
    {
        yield return new WaitForSeconds(0.2f);
        Button DeleteButton = Modal.transform.GetChild(1).GetChild(3).GetChild(0).GetChild(0).GetComponent<Button>();

        int i = 0;
        foreach (Transform child in transform)
        {
            if(child.gameObject == Button)
            {
                int j = i-1; //Because the title gameobject is also a child of the transform
                DeleteButton.onClick.AddListener(delegate { gameObject.GetComponent<PromoCodeData>().deletePromoCode(j); });
            }
            i++;
        }
        yield return null;
    }

    /// <summary>
    /// Take a Button and a Modal and bind the modal apparition to the button component onClick method
    /// </summary>
    /// <param name="Modal">The Modal gameobject</param>
    /// <param name="Button">The Button gameobject to be linked to the modal</param>
    public void linkButtonModal(GameObject Button, GameObject Modal)
    {
        Button.GetComponent<Button>().onClick.AddListener(Modal.GetComponent<ModalWindowManager>().OpenWindow);
    }

    /// <summary>
    /// Manage the functions to instantiate all requirement to create successfully a new promo code on our application view
    /// </summary>
    /// <param name="ButtonText">The wanted text on our button as string</param>
    /// <param name="ModalTitle">The wanted text on our modal title as string</param>
    /// <param name="ModalFirstLine">The wanted text on our modal first line as string</param>
    /// <param name="ModalSecondLine">The wanted text on our modal second line as string</param>
    public void appendPromoCode(string ButtonText, string ModalTitle, string ModalFirstLine = null, string ModalSecondLine = null)
    {
        GameObject Button = instantiateButton();
        GameObject Modal = instantiateModal();
        linkButtonModal(Button, Modal);
        settingButton(Button, ButtonText);
        settingModal(Modal, ModalTitle, ModalFirstLine, ModalSecondLine);
        StartCoroutine(bindModalDeleteButton(Modal, Button));
    }

    /// <summary>
    /// Responsible to destroy every elements relative to instantiated promo code on our application view.
    /// </summary>
    public void destroyAll()
    {
        foreach (Transform child in transform)
        {
            if (child.name != "Title")
            {
                Destroy(child.gameObject);
            }
        }
        foreach (Transform child in ModalContainer.transform)
        {
            Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Responsible for updating the UI Promo Code view on our application so it follow the addition and deletion of promo codes.
    /// </summary>
    public IEnumerator resizeGridUI()
    {
        yield return new WaitForEndOfFrame();
        LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.transform.parent.GetComponent<RectTransform>());
        yield return null;
    }
}

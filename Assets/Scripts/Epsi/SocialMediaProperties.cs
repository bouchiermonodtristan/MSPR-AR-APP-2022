using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SocialMediaProperties
{

    [SerializeField]
    private SocialMediaMessage properties;


    public SocialMediaProperties(SocialMediaMessage Properties)
    {
        properties = Properties;
    }


    public SocialMediaMessage Properties { get => properties; set => properties = value; }
}

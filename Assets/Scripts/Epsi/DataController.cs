﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;

/// <summary>
/// Represents the access to the localStorage and every way to do it.
/// </summary>
public class DataController : MonoBehaviour
{
    /// <summary>
    /// Save the prefs localy on the local storage
    /// </summary>
    public void PlayerPrefSave()
    {
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Can the localstorage return a saved value using the specified name
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <returns>The response as a boolean</returns>
    public bool isPlayerPref(string name)
    {
        return PlayerPrefs.HasKey(name);
    }

    /// <summary>
    /// Delete everything on the local storage of the device
    /// </summary>
    public void PlayerPrefDeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }

    /// <summary>
    /// Write the given int to the local storage using the given key string
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <param name="number">The int that will be written on the local storage</param>
    public void PlayerPrefSetInt(string name, int number)
    {
        PlayerPrefs.SetInt(name, number);
    }

    /// <summary>
    /// Return a saved int using the specified name
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <returns>The response as a int</returns>
    public int PlayerPrefGetInt(string name)
    {
        return PlayerPrefs.GetInt(name);
    }

    /// <summary>
    /// Write the given float to the local storage using the given key string
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <param name="number">The float that will be written on the local storage</param>
    public void PlayerPrefSetFloat(string name, float number)
    {
        PlayerPrefs.SetFloat(name, number);
    }

    /// <summary>
    /// Return a saved float using the specified name
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <returns>The response as a float</returns>
    public float PlayerPrefGetFloat(string name)
    {
        return PlayerPrefs.GetFloat(name);
    }

    /// <summary>
    /// Write the given string to the local storage using the given key string
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <param name="number">The string that will be written on the local storage</param>
    public void PlayerPrefSetString(string name, string stringValue)
    {
        PlayerPrefs.SetString(name, stringValue);
    }

    /// <summary>
    /// Return a saved string using the specified name
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <returns>The response as a string</returns>
    public string PlayerPrefGetString(string name)
    {
        return PlayerPrefs.GetString(name);
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible for containing a list of all our promo code objects.
/// </summary>
[Serializable]
public class PromoCodeModelList
{
    /// <summary>
    /// Responsible for listing all our Promo Code objects.
    /// </summary>
    [SerializeField]
    private List<PromoCodeModel> list;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="List">The list containing all our promo code objects</param>
    /// <returns>Return an instance of this</returns>
    public PromoCodeModelList(List<PromoCodeModel> List)
    {
        this.List = List;
    }

    /// <summary>
    /// Setter and getter for the List of promo code.
    /// </summary>
    public List<PromoCodeModel> List { get => list; set => list = value; }
}

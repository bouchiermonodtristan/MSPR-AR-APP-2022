﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible for the model of our promo code objects
/// </summary>
[Serializable]
public class PromoCodeModel
{
    /// <summary>
    /// The title of our button
    /// </summary>
    [SerializeField]
    private string buttonTittle;

    /// <summary>
    /// The title of our modal
    /// </summary>
    [SerializeField]
    private string modalTittle;

    /// <summary>
    /// The first line of our modal
    /// </summary>
    [SerializeField]
    private string modalFirstLine;

    /// <summary>
    /// The second line of our modal
    /// </summary>
    [SerializeField]
    private string modalSecondLine;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="ButtonTittle">The wanted text on our button as string</param>
    /// <param name="ModalTittle">The wanted text on our modal title as string</param>
    /// <param name="ModalFirstLine">The wanted text on our modal first line as string</param>
    /// <param name="ModalSecondLine">The wanted text on our modal second line as string</param>
    /// <returns>Return an instance of this</returns>
    public PromoCodeModel(string ButtonTittle, string ModalTittle, string ModalFirstLine, string ModalSecondLine)
    {
        buttonTittle = ButtonTittle;
        modalTittle = ModalTittle;
        modalFirstLine = ModalFirstLine;
        modalSecondLine = ModalSecondLine;
    }

    /// <summary>
    /// Setter and Getter for our button title text
    /// </summary>
    public string ButtonTittle { get => buttonTittle; set => buttonTittle = value; }

    /// <summary>
    /// Setter and Getter for our modal title text
    /// </summary>
    public string ModalTittle { get => modalTittle; set => modalTittle = value; }

    /// <summary>
    /// Setter and Getter for our modal first line text
    /// </summary>
    public string ModalFirstLine { get => modalFirstLine; set => modalFirstLine = value; }

    /// <summary>
    /// Setter and Getter for our modal second line text
    /// </summary>
    public string ModalSecondLine { get => modalSecondLine; set => modalSecondLine = value; }
}

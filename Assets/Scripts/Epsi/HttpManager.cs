﻿using CI.HttpClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using Michsky.UI.ModernUIPack;

/// <summary>
/// Represents the access to our API server.
/// </summary>
public class HttpManager : MonoBehaviour
{
    /// <summary>
    /// The link to the scene object parent of the promo codes.
    /// This reference will be used to access the promo codes data script.
    /// </summary>
    public SocialMediaForm socialMediaForm;
    public string serverEndpoint = "https://api.hubapi.com/crm/v3/objects/contacts";
    public string apiKey = "eu1-c092-efc9-4718-898f-d2c66dbe2eee";
    public string responseData = "";

    public void httpPostSocialMedia(SocialMediaProperties message)
    {
        HttpClient client = new HttpClient();

        StringContent content = new StringContent(JsonUtility.ToJson(message), Encoding.UTF8, "application/json");


        client.Post(new System.Uri(serverEndpoint + "?hapikey=" + apiKey), content, HttpCompletionOption.AllResponseContent, (r) =>
        {
#pragma warning disable 0219
            responseData = r.ReadAsString();
            Debug.Log("Response received from HubSpot");
            Debug.Log(responseData);
            socialMediaForm.StartCoroutine(socialMediaForm.messageReceived(r));
#pragma warning restore 0219
        });
    }

}

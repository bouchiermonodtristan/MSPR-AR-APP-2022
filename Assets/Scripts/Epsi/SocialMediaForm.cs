using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;
using TMPro;
using CI.HttpClient;
using static Michsky.UI.ModernUIPack.HorizontalSelector;
using System.Text.RegularExpressions;

public class SocialMediaForm : MonoBehaviour
{
    public ModalWindowManager myModal;
    public ModalWindowManager loadingScreen = null;
    public NotificationManager notificationManager = null;
    public ModalWindowManager modaleRetourAPI = null;
    public TMP_InputField ChampsNom;
    public TMP_InputField ChampsPrenom;
    public TMP_InputField ChampsTelephone;
    public TMP_InputField ChampsEmail;
    public GameObject errorNom;
    public GameObject errorPrenom;
    public GameObject errorTelephone;
    public GameObject errorEmail;
    public HttpManager httpManager;

    #region tests
    public bool areEveryfieldsOk()
    {
        isNomOk();
        isPrenomOk();
        isTelephoneOk();
        isEmailOk();
        if (isNomOk() && isPrenomOk() && isTelephoneOk() && isEmailOk())
        {
            return true;
        }
        return false;
    }

    public bool isNomOk()
    {
        if(ChampsNom.text.Length >= 2)
        {
            return true;
        }
        errorNom.SetActive(true);
        return false;
    }

    public bool isPrenomOk()
    {
        if (ChampsPrenom.text.Length >= 2)
        {
            return true;
        }
        errorPrenom.SetActive(true);
        return false;
    }

    public bool isTelephoneOk()
    {
        if (IsPhoneNumber(ChampsTelephone.text))
        {
            return true;
        }
        errorTelephone.SetActive(true);
        return false;
    }

    public bool isEmailOk()
    {
        if (IsEmail(ChampsEmail.text))
        {
            return true;
        }
        errorEmail.SetActive(true);
        return false;
    }

    public bool IsPhoneNumber(string number)
    {
        return Regex.Match(number, @"^([0-9]{10})$").Success;
    }
    public bool IsEmail(string mail)
    {
        return Regex.Match(mail, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success;
    }
   
    #endregion

    public void trySubmit()
    {
        removeErrorArrows();
        if (areEveryfieldsOk())
        {
            StartCoroutine(sendHttpPostToSocialMedia());
        }else
        {
            notificationManager.OpenNotification();
        }
    }

    public IEnumerator sendHttpPostToSocialMedia()
    {
        myModal.CloseWindow();
        loadingScreen.OpenWindow();
        yield return new WaitForEndOfFrame();


        var message = new SocialMediaProperties(new SocialMediaMessage(ChampsNom.text, ChampsPrenom.text, ChampsTelephone.text, ChampsEmail.text));

        httpManager.httpPostSocialMedia(message);
    }

    public void setModaleRetourAPI(HttpResponseMessage message)
    {
        if(message.IsSuccessStatusCode)
        {
            modaleRetourAPI.titleText = "F�licitation !";
            modaleRetourAPI.descriptionText = "Vous pouvez vous connecter aux reseaux sociaux.";
        }
        else
        {
            modaleRetourAPI.titleText = "Oups. Erreur !";
            modaleRetourAPI.descriptionText = "Cet email existe d�j� en base<br>Mais vous pouvez quand m�me cliquer ci dessous.";
        }
        modaleRetourAPI.UpdateUI();
    }

    public IEnumerator messageReceived(HttpResponseMessage message)
    {
        yield return new WaitForSeconds(2);
        loadingScreen.CloseWindow();
        yield return new WaitForEndOfFrame();
        setModaleRetourAPI(message);
        modaleRetourAPI.OpenWindow();
    }

    public void autofill()
    {
        ChampsNom.text = "DebugJurry";
        ChampsPrenom.text = "DebugJurry2";
        ChampsTelephone.text = "0666666666";
        ChampsEmail.text = "debugjurry666@gmail.com";
    }

    public void removeErrorArrows()
    {
        errorNom.SetActive(false);
        errorPrenom.SetActive(false);
        errorTelephone.SetActive(false);
        errorEmail.SetActive(false);
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents the communication between our local storage and our application promo code list.
/// Ensure that the application promo codes and the local storage will be synchronized.
/// </summary>
public class PromoCodeData : MonoBehaviour
{
    /// <summary>
    /// The link to our dataController script in order to access the local storage of the device. 
    /// </summary>
    public DataController dataController;

    /// <summary>
    /// Our object list of promo code. Contain all the promo code objets in promoCodeList.List.
    /// </summary>
    public PromoCodeModelList promoCodeList;

    /// <summary>
    /// The string used to write and read the local storage.
    /// </summary>
    private string keyWord = "QRCODE";

    /// <summary>
    /// Called at startup
    /// If we have data on our local storage, it will load them and show them in the application
    /// </summary>
    private void Start()
    {
        if (isLocalStorageExisting())
        {
            unserializePromosCodes();
        }

        printAllPromoCode();
    }

    /// <summary>
    /// Get all code from the promoCodeList and show them in the application
    /// </summary>
    public void printAllPromoCode()
    {
        foreach (PromoCodeModel promoCodeModel in promoCodeList.List)
        {
            gameObject.GetComponent<PromoCodeManager>().appendPromoCode(promoCodeModel.ButtonTittle, promoCodeModel.ModalTittle, promoCodeModel.ModalFirstLine, promoCodeModel.ModalSecondLine);
        }
    }

    /// <summary>
    /// Can the localstorage return a saved value using the specified keyWork string
    /// </summary>
    /// <param name="name">The key string to access local storage matching value</param>
    /// <returns>The response as a boolean</returns>
    public bool isLocalStorageExisting()
    {
        return dataController.isPlayerPref(keyWord);
    }

    /// <summary>
    /// Get all promo code objects from local storage and put them on our list of promo code
    /// </summary>
    public void unserializePromosCodes()
    {
        promoCodeList = JsonUtility.FromJson<PromoCodeModelList>(dataController.PlayerPrefGetString(keyWord));
    }

    /// <summary>
    /// Get all promo code objects from our list of promo code and serialize them to the local storage
    /// </summary>
    public void serializePromosCodes()
    {
        dataController.PlayerPrefSetString(keyWord, JsonUtility.ToJson(promoCodeList));
    }

    /// <summary>
    /// Take a PromoCodeModel object and add it in the local storage and PromoCode list.
    /// Renew the application view with the new datas.
    /// </summary>
    /// <param name="promoCodeModel">The PromoCodeModel object that will be added to our application view and local storage</param>
    public void addPromoCode(PromoCodeModel promoCodeModel)
    {
        gameObject.GetComponent<PromoCodeManager>().destroyAll();
        promoCodeList.List.Add(promoCodeModel);
        printAllPromoCode();
        StartCoroutine(gameObject.GetComponent<PromoCodeManager>().resizeGridUI());
        serializePromosCodes();
        dataController.PlayerPrefSave();
    }

    /// <summary>
    /// Take an integer position and the corresponding PromoCode from both our PromoCodeList and local storage
    /// Renew the application view with the new datas.
    /// </summary>
    /// <param name="i">The Promo Code GameObject position in our application view list passed as int</param>
    public void deletePromoCode(int i)
    {
        gameObject.GetComponent<PromoCodeManager>().destroyAll();
        promoCodeList.List.RemoveAt(i);
        printAllPromoCode();
        StartCoroutine(gameObject.GetComponent<PromoCodeManager>().resizeGridUI());
        serializePromosCodes();
        dataController.PlayerPrefSave();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetupSelenium : MonoBehaviour
{
    public Text myText;

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("AutoPlayAutomation") == null)
        {
         //   DontDestroyOnLoad(new GameObject("AutoPlayAutomation").AddComponent<AutoPlay.Unity.StartPlay>());
        }
        myText = GameObject.FindGameObjectWithTag("SeleniumText").GetComponent<Text>();
    }


    public void setSeleniumButtonText(string text)
    {
        myText.text = text;
    }
}

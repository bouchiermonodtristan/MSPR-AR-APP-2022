﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;
using static Michsky.UI.ModernUIPack.HorizontalSelector;
using System;
#if USE_ARFOUNDATION
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
#endif

public class ARFoundationColorMapping : MonoBehaviour
{
#if USE_ARFOUNDATION
    public ARTrackedImageManager imageManager;
#endif

    public GameObject MonkeyPrefab = null;
    public GameObject SnakePrefab = null;
    public GameObject HippoPrefab = null;
    public HorizontalSelector selector = null;
    public NotificationManager notificationManagerFail = null;
    public NotificationManager notificationManagerSuccess = null;
    public ModalWindowManager loadingScreen = null;
    public bool isPrecis = false;


    public RenderTexture rt;
    public int realWidth;
    public int realHeight;

    private GameObject arContents;
    private GameObject drawObj;

    private GameObject cube;

    public bool isStart = false;
    public bool isEnd = false;

    public Text timeText;
    private float time;
    private GameObject instantiatedPrefab = null;


    private void Update()
    {
        if (isStart)
        {
            time += Time.deltaTime;
        }

        if (isEnd)
        {
            Debug.Log("Finished to compute image within " + (time * 1000).ToString() + "ms");

            time = 0.0f;
            isEnd = false;
        }
    }

#if USE_ARFOUNDATION
    void Start()
    {
        imageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        ARTrackedImage trackedImage = null;

        for (int i = 0; i < eventArgs.added.Count; i++)
        {
            trackedImage = eventArgs.added[i];

            string imgName = trackedImage.referenceImage.name;
            if (imgName == "Monkey" && (instantiatedPrefab != MonkeyPrefab || instantiatedPrefab == null))
            {
                if (arContents != null)
                    Destroy(arContents);

                instantiatedPrefab = MonkeyPrefab;
                arContents = Instantiate(MonkeyPrefab, trackedImage.transform);
                Debug.Log(trackedImage.transform.localPosition.x);
                Debug.Log(trackedImage.transform.localPosition.y);
                cube = CreateCubeForARFoundationTarget(arContents.gameObject, trackedImage.size.x, trackedImage.size.y);
            } 
            else if (imgName == "Snake" && (instantiatedPrefab != SnakePrefab || instantiatedPrefab == null))
            {
                if (arContents != null)
                    Destroy(arContents);

                instantiatedPrefab = SnakePrefab;
                arContents = Instantiate(SnakePrefab, trackedImage.transform);
                Debug.Log(trackedImage.transform.localPosition.x);
                Debug.Log(trackedImage.transform.localPosition.y);
                cube = CreateCubeForARFoundationTarget(arContents.gameObject, trackedImage.size.x, trackedImage.size.y);
            }
            else if (imgName == "Rino" && (instantiatedPrefab != HippoPrefab || instantiatedPrefab == null))
            {
                if (arContents != null)
                    Destroy(arContents);

                instantiatedPrefab = HippoPrefab;
                arContents = Instantiate(HippoPrefab, trackedImage.transform);
                Debug.Log(trackedImage.transform.localPosition.x);
                Debug.Log(trackedImage.transform.localPosition.y);
                cube = CreateCubeForARFoundationTarget(arContents.gameObject, trackedImage.size.x, trackedImage.size.y);
            }
        }

     /*   for (int i = 0; i < eventArgs.updated.Count; i++)
        {
            trackedImage = eventArgs.updated[i];

            if (trackedImage.trackingState == TrackingState.Tracking)
            {
                arContents.SetActive(true);
            }
            else
            {
                arContents.SetActive(false);
            }
        }

        for (int i = 0; i < eventArgs.removed.Count; i++)
        {
            arContents.SetActive(false);
        }*/
    }
#endif
    public void PlayFast()
    {
        if (!isEnd && !isStart)
        {
            isStart = true;

            bool isSuccess = false;
            try
            {
                Texture2D screenShotTex = ScreenShot.GetScreenShot(arContents);

                drawObj = GameObject.FindGameObjectWithTag("coloring");

                drawObj.GetComponent<Renderer>().material.mainTexture = screenShotTex;
                notificationManagerSuccess.OpenNotification();
                isSuccess = true;
            }
            catch(Exception e)
            {

            }
            if(!isSuccess)
            {
                notificationManagerFail.OpenNotification();
            }
            isStart = false;
            isEnd = true;
        }
    }

    public void PlayPrecis()
    {
        if (!isEnd && !isStart && cube)
        {
            isStart = true;


            float[] srcValue = AirarManager.Instance.CalculateMarkerImageVertex(cube);
            Texture2D screenShotTex = ScreenShot.GetScreenShot(arContents);
            bool isSuccess = false;
            try {
                AirarManager.Instance.ProcessColoredMapTexture(screenShotTex, srcValue, realWidth, realHeight, (resultTex) =>
                {
                    drawObj = GameObject.FindGameObjectWithTag("coloring");
                    drawObj.GetComponent<Renderer>().material.mainTexture = resultTex;
                    notificationManagerSuccess.OpenNotification();
                    isSuccess = true;
                });
            } 
            catch (NullReferenceException e)
            {
            }
            if(!isSuccess)
            {
                notificationManagerFail.OpenNotification();
            }
            isStart = false;
            isEnd = true;
        }
    }

    public void resetTimer()
    {
        isStart = false;
        isEnd = true;
    }

    public void setIndex(int index)
    {
        Debug.Log(index);
        if(index == 0)
        {
            isPrecis = false;
        }else if(index == 1)
        {
            isPrecis = true;
        }
        resetTimer();
    }

    public void Play()
    {
        if(!isPrecis)
        {
            PlayFast();
        }else if(isPrecis)
        {
            PlayPrecis();
        }
    }

    /// <summary>
    /// Create a full size cube on the ARFoundation marker image
    /// </summary>
    /// <param name="targetWidth">marker image width</param>
    /// <param name="targetHeight">marker image height</param>
    public GameObject CreateCubeForARFoundationTarget(GameObject parentObj, float targetWidth, float targetHeight)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.GetComponent<Renderer>().material = AirarManager.Instance.transparentMat;
        //cube.GetComponent<Renderer>().material.color = Color.green;
        cube.transform.SetParent(parentObj.transform);
        cube.transform.localPosition = Vector3.zero;
        cube.transform.localRotation = Quaternion.Euler(Vector3.zero);
        cube.transform.localScale = new Vector3(targetWidth, 0.001f, targetHeight);

        return cube;
    }
}

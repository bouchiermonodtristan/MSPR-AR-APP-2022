# MSPR-AR-APP-2022

Le versioning de l'application mobile.

![Demo Scene](Images/CEREALIS.jpg)

[Video de demo](https://drive.google.com/file/d/1Yl_kEK7D_AHAG6Dn5gyw7kLx2LbGzuiP/view?usp=sharing)

## Pour jouer
Installez l'APK Android sur votre t�l�phone. Elle se trouve dans le dossier Build/build.apk
L'application n'a pas �t� build pour IOS, mais vous pouvez la build vous m�me en suivant les suggestions ci dessous.

### Dans l'application pour l'UI :
<p>L'appareil photo sert a propager les couleurs<br>
Le bouton partager � requeter notre CRM</p>
<ins>Le bouton couleur propose un choix entre deux algorithmes :</ins>
<p>
Le <strong>pr�cis</strong> retrouve les couleurs pour les mettre bien sur le model mais demande � bien viser l'image. <br>
Le <strong>rapide</strong> n'a pas besoin de trouver l'image et se contente de mettre le contenu de la camera sur le model.<br>
(Voir l'UI plus bas)
</p>
![Demo Scene](Images/snakein.jpg)
![Demo Scene](Images/menuin.jpg)

## Pour installer le projet
Pour installer 4 �tapes :

<ol>
  <li>Telechargez le zip complet du projet � son niveau de premier commit [� ce lien](https://drive.google.com/file/d/1MZp3eHT930BF2szzHrOn7gfRd6giobLG/view?usp=sharing)</li>
  <li>Initialisez le repository git sur votre machine.</li>
  <li>D�zippez le projet telecharg� dans le repository initialis�.</li>
  <li>Pull la branche master dans le repository pour �craser le projet avec les derni�res modifications.</li>
</ol>

## Pour d�velopper
Version de Unity = 2020.3.0f1. (Ou plus !)

## Images support�es
<p>
Vous pouvez les flasher avec votre t�l�phone dans l'application AR.
Ou bien rajouter les votre directement dans Unity une fois le projet install�.
</p>

![Monkey](Images/Monkey.PNG)
![Monkey Colorized](Images/MonkeyColorized.png)
![Rhinoceros](Images/Rino.PNG)
![Rhinoceros Colorized](Images/RinoColorized.PNG)
![Snake](Images/Snake.PNG)
![Snake Colorized](Images/SnakeColorized.png)